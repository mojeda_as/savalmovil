export class Fixedasset {
    $key: string;
    
    fechaCreacion: string;
    creadoPor: string;
    codigo: string;
    codigoRfid: string;
    claseActivoFijo: string;
    sociedad: string;
    especie: string;
    gestionHistorica: string;
    denominacion: string;
    listaInventario: string;
    cuenta: string;
    centroCosto: string;
    centro: string;
    area: string;
    edificio: string;
    sala: string;
    piso: string;
    acreedor: string;
    fabricante: string;
    condicion: string;
    agnosVidaUtil: string;
}
