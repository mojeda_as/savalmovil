import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { FixedassetsComponent } from './components/fixedassets/fixedassets.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'home', component: HomeComponent},
  { path: 'fixedAssets', component: FixedassetsComponent },
  { path: 'fixedAssetCreate', component: FixedassetsComponent},
  { path: 'fixedAssetMove', component: FixedassetsComponent },
  { path: 'fixedAssetDelete', component: FixedassetsComponent },
  { path: 'fixedAssetUpdate', component: FixedassetsComponent },
  { path: 'fixedAssetDetail', component: FixedassetsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

