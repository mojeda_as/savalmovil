import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

import { FixedassetService } from '../../../services/fixedasset.service';

import { Fixedasset } from 'src/app/models/fixedasset';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-fixedasset',
  templateUrl: './fixedasset.component.html',
  styleUrls: ['./fixedasset.component.scss']
})

export class FixedassetComponent implements OnInit {

  constructor( 
    public fixedassetService: FixedassetService,
    public toastr: ToastrService
    ) { }

  ngOnInit(){
    this.fixedassetService.getFixedAssets()
    this.resetFrom();
  }
  
  onSubmit(fixedAssetForm: NgForm)
  {  
    if(fixedAssetForm.value.$key == null)
    this.fixedassetService.insertFixedAsset(fixedAssetForm.value);
  else
    this.fixedassetService.updateFixedAsset(fixedAssetForm.value);
  
    this.resetFrom(fixedAssetForm);
    this.toastr.success('Succeful operation', 'Successful Operation') 
  }
    
 resetFrom( fixedAssetForm?: NgForm )
 {
  if(fixedAssetForm != null ){
    fixedAssetForm.reset();
    this.fixedassetService.selectedFixedasset = new Fixedasset;
  }
 }

}


