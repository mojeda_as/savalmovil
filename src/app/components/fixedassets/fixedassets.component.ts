import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fixedassets',
  templateUrl: './fixedassets.component.html',
  styleUrls: ['./fixedassets.component.scss']
})
export class FixedassetsComponent implements OnInit {

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  hasRoute(route: string) {
    return this.router.url.includes(route);
  }

}
