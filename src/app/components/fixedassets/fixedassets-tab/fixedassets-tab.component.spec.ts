import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedassetsTabComponent } from './fixedassets-tab.component';

describe('FixedassetsTabComponent', () => {
  let component: FixedassetsTabComponent;
  let fixture: ComponentFixture<FixedassetsTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedassetsTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedassetsTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
