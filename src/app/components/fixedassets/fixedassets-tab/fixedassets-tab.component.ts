import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fixedassets-tab',
  templateUrl: './fixedassets-tab.component.html',
  styleUrls: ['./fixedassets-tab.component.scss']
})
export class FixedassetsTabComponent implements OnInit {

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  hasRoute(route: string) {
    return this.router.url.includes(route);
  }

}
