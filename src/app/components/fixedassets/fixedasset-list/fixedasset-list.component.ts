import { Component, OnInit } from '@angular/core';
//service
import { FixedassetService } from '../../../services/fixedasset.service';
//class
import {Fixedasset} from '../../../models/fixedasset';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-fixedasset-list',
  templateUrl: './fixedasset-list.component.html',
  styleUrls: ['./fixedasset-list.component.scss']
})
export class FixedassetListComponent implements OnInit {

  fixedassetsList : Fixedasset[];

  constructor(
    public fixedassetService : FixedassetService,
    public toastr: ToastrService
  ) { }

ngOnInit() {

this.fixedassetService.getFixedAssets()
.snapshotChanges()
.subscribe(
  item => {
    this.fixedassetsList =[]; 
   item.forEach(element => {
          let x = element.payload.toJSON();
          x["$key"] = element.key;
          this.fixedassetsList.push(x as Fixedasset);
        });
  });
  }


  onEdit( fixedasset: Fixedasset) {
    this.fixedassetService.selectedFixedasset = Object.assign({}, fixedasset);
  }

  onDelete($key: string) {
    if(confirm('Are you sure you want to delete it?')) {
      this.fixedassetService.deleteFixedAsset($key);
      this.toastr.warning('Deleted Successfully', 'Product Removed');
    }
  }





}
