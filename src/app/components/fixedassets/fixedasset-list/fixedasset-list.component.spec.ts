import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedassetListComponent } from './fixedasset-list.component';

describe('FixedassetListComponent', () => {
  let component: FixedassetListComponent;
  let fixture: ComponentFixture<FixedassetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedassetListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedassetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
