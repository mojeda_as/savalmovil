import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedassetsComponent } from './fixedassets.component';

describe('FixedassetsComponent', () => {
  let component: FixedassetsComponent;
  let fixture: ComponentFixture<FixedassetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedassetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedassetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
