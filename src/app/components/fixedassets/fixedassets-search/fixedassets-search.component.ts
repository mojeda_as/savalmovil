import { Component, OnInit } from '@angular/core';
//service
import { FixedassetService } from '../../../services/fixedasset.service';
//class
import {Fixedasset} from '../../../models/fixedasset';

@Component({
  selector: 'app-fixedassets-search',
  templateUrl: './fixedassets-search.component.html',
  styleUrls: ['./fixedassets-search.component.scss']
})
export class FixedassetsSearchComponent implements OnInit {


fixedassetsList : Fixedasset[];

  constructor(
    public fixedassetService : FixedassetService,
  ) { }



  ngOnInit() {
    this.fixedassetService.getFixedAssets()
    .snapshotChanges()
    .subscribe(
        item => {
          this.fixedassetsList =[];  
          item.forEach(element => {
                let x = element.payload.toJSON();
                x["$key"] = element.key;
                this.fixedassetsList.push(x as Fixedasset);
              });
        });
      }

      onEdit( fixedasset: Fixedasset) {
        this.fixedassetService.selectedFixedasset = Object.assign({}, fixedasset);

      }
    
    

}
