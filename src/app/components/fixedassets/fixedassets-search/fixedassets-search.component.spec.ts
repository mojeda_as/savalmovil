import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedassetsSearchComponent } from './fixedassets-search.component';

describe('FixedassetsSearchComponent', () => {
  let component: FixedassetsSearchComponent;
  let fixture: ComponentFixture<FixedassetsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedassetsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedassetsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
