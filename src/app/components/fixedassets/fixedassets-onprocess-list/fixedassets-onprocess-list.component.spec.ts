import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedassetsOnprocessListComponent } from './fixedassets-onprocess-list.component';

describe('FixedassetsOnprocessListComponent', () => {
  let component: FixedassetsOnprocessListComponent;
  let fixture: ComponentFixture<FixedassetsOnprocessListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedassetsOnprocessListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedassetsOnprocessListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
