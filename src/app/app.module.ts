import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';

//Firebase
import{AngularFireModule} from 'angularfire2';
import{AngularFireDatabaseModule} from 'angularfire2/database';
import { environment } from '../environments/environment';
//Components
import { FixedassetsComponent } from './components/fixedassets/fixedassets.component';
import { FixedassetListComponent } from './components/fixedassets/fixedasset-list/fixedasset-list.component';
import { FixedassetComponent } from './components/fixedassets/fixedasset/fixedasset.component';
//Services
import { FixedassetService } from './services/fixedasset.service';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { FixedassetsOnprocessListComponent } from './components/fixedassets/fixedassets-onprocess-list/fixedassets-onprocess-list.component';
import { FixedassetsTabComponent } from './components/fixedassets/fixedassets-tab/fixedassets-tab.component';
import { FixedassetsSearchComponent } from './components/fixedassets/fixedassets-search/fixedassets-search.component';


@NgModule({
  declarations: [
    AppComponent,
    FixedassetsComponent,
    FixedassetListComponent,
    FixedassetComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    FixedassetsOnprocessListComponent,
    FixedassetsTabComponent,
    FixedassetsSearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [FixedassetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
