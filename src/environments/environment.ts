// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: { 
    apiKey: "AIzaSyAdmq8IbTJNVxzJ4S2d-MbgLhTBPYKeA6M",
    authDomain: "savalapp.firebaseapp.com",
    databaseURL: "https://savalapp.firebaseio.com",
    projectId: "savalapp",
    storageBucket: "savalapp.appspot.com",
    messagingSenderId: "222573123075",
    appId: "1:222573123075:web:662b83909e895791686036"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
